#' @title Data warehouse connection
#' @description Creates connection to data warehouse
#' @author Jani Miettinen <jani.k.miettinen@gmail.com>
#' @param db select database from "STATTLESHIP", "GROOT", "USERPROD"
#' @param user username to connect database
#' @param password for your db connection
#' @param encoding connection encoding, ex. LATIN9, WINDOWS-1252, UTF-8
#' @examples
#'  \dontrun{
#'  conn <- dw_connection("USERPROD")
#'  dw_disconnect(conn)
#'  }
#' @rdname dw_connection
#' @export

dw_connection <- function(db = "STATTLESHIP", user = "", password = "", encoding = NULL){

  # Check sys.info and determine encoding:
  # TODO ADD ENCODING TO connection and Test!
  if(is.null(encoding) & Sys.info()[[1]] == "Windows") encoding = "WINDOWS-1252"
  if(is.null(encoding) & Sys.info()[[1]] == "Linux") encoding = "UTF-8"
  if(is.null(encoding) & Sys.info()[[1]] == "Darwin") encoding = "UTF-8"

  # Few checks first
  db <- toupper(db)
  # if (!db %in% c("STATTLESHIP","GROOT", "GROOTADMIN", "STATTLESHIPADMIN", "USERPROD")) stop("Check db name")
  # if (db %in% "PROJPROD" & schema == "") stop("You must define schema name for PROJPROD database")
  if(user == ""){
    if (nchar(Sys.getenv("USERNAME")) < 3 | Sys.getenv("USERNAME") == "tunnus") {
      Sys.setenv(USERNAME = .rs.askForPassword("Anna tunnuksesi:"))
    }
    user <- Sys.getenv("USERNAME")
  }
  if(password == ""){
    if (nchar(Sys.getenv("PASSWD")) < 3 | Sys.getenv("PASSWD") == "salasana") {
      Sys.setenv(PASSWD = .rs.askForPassword("Anna salasana:"))
    }
    password <- Sys.getenv("PASSWD")
  }

  conn <- odbc::dbConnect(
    drv = RMariaDB::MariaDB(),
    user = user,
    password = password,
    host = "192.168.2.220",
    port = 3306,
    dbname = db,
    encoding = encoding
  )

  return(conn)
}


