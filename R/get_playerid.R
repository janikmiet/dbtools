#' @title Get player id from stattleship database
#' @description Returns player id
#' @author Jani Miettinen <jani.k.miettinen@gmail.com>
#' @param name string. Give player name
#' @examples
#'  \dontrun{
#' get_playerid()
#'  }
#' @rdname get_playerid
#' @export
#'
## SQL PLAYER_ID id by name
get_playerid <- function(name="Lauri Markkanen"){
  dbtools::gq(paste0("SELECT ID FROM PLAYERS WHERE NAME='",name,"'; "))[[1]] # TEAM ID
}
