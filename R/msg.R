#' @title Print a Message
#' @description Prints a message to console with or without timestamp
#' @author Jani Miettinen <jani.k.miettinen@gmail.com>
#' @param msg string. Message.
#' @param time boolean. Print time stamp
#' @examples
#' msg()
#'
#' @rdname msg
#' @export
#'
msg <- function(msg, time=TRUE){
  msg <- msg
  if(time){
    print(paste0(Sys.time(), " ", msg))
  }else{
    print(paste0(msg))
  }
}
