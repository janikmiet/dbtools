#' @title Get sql query
#' @description returns outcome of sql query from database
#' @param sql string. SQL query
#' @param group database connection alias
#' @author Jani Miettinen
#' @return dataframe
#' @examples
#'  \dontrun{
#'  get_query("SELECT 1")
#'  }
#' @rdname get_query
#' @rdname gq
#' @export
get_query <- function(sql, group="STATTLESHIP"){
  con <- odbc::dbConnect(RMariaDB::MariaDB(), group=group)
  d <- odbc::dbGetQuery(con, sql)
  d <- dplyr::as_data_frame(d)
  suppressWarnings(odbc::dbDisconnect(con))
  return(d)
}

#' @title Get sql query
#' @description returns outcome of sql query from database
#' @param sql string. SQL query
#' @param group database connection alias
#' @author Jani Miettinen
#' @return dataframe
#' @examples
#'  \dontrun{
#'  gq("SELECT 1")
#'  }
#' @rdname gq
#' @export
gq <- function(sql, group="STATTLESHIP"){
  con <- odbc::dbConnect(RMariaDB::MariaDB(), group=group)
  d <- odbc::dbGetQuery(con, sql)
  d <- dplyr::as_data_frame(d)
  suppressWarnings(odbc::dbDisconnect(con))
  return(d)
}
