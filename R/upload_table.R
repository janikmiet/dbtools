#' @title Upload dataframe to MariaDB
#' @description Message print to console with or without timestamp
#' @author Jani Miettinen <jani.k.miettinen@gmail.com>
#' @param object data frame
#' @param group database connection
#' @param overwrite overwrite table in database
#' @param append add rows to table in database
#' @examples
#' upload_table()
#'
#' @rdname upload_table
#' @export
#'
#'
upload_table <- function(object,
                      group = NULL,
                      overwrite = F,
                      append = T){
  ## Tarkistetaan objekti ja editoidaan object kantaan sopivaksi
  # row.names(object) <- NULL
  object <- dplyr::as_data_frame(object)
  stopifnot(
    is.data.frame(object),
    is.logical(overwrite),
    is.logical(append)
  )
  filename <- toupper(deparse(substitute(object))) # Filename
  names(object) <- toupper(names(object))
  msg <- paste0(Sys.time(), " Writing ", filename, " (", nrow(object) ," rows) to ", group)
  print(msg)
  con <- odbc::dbConnect(RMariaDB::MariaDB(), group = group)
  odbc::dbWriteTable(con, filename, object, overwrite=overwrite, append=append)
  odbc::dbDisconnect(con)
  msg <- paste0(Sys.time(), " Connection closed.") # Log and msg
  print(msg)
}

# write_table <- function(object, name, overwrite = FALSE, append = FALSE, database = "USERPROD", schema = ""){
#
#   ## Tarkistetaan objekti ja editoidaan object kantaan sopivaksi
#   row.names(object) <- NULL
#   object <- dplyr::as_data_frame(object)
#   stopifnot(
#     is.data.frame(object),
#     is.character(name),
#     is.logical(overwrite),
#     is.logical(append)
#   )
#   # TODO tämä ei jostain syystä toimi oikein Kela-ympäristössä:
#   # Halutaan vain object:n nimi
#   # filename <- deparse(substitute(object))
#   # filename <- toupper(filename)
#   names(object) <- toupper(names(object))
#   name <- toupper(name)
#   # TODO : No row.names . How to check that there is no row nameS?
#   # if( any(row.names(object) != as.character(seq(1,length(row.names(object))))) ){
#   #   object$ROWNAMES <- row.names(object)
#   #   rownames(object) <- c()
#   # }
#   ## Yhteys tietovarastoon
#   conn <- dbtools::dw_connection(db = database, schema = schema)
#   ## ladataan objekti tietovarastoon ja suljetaan yhteys
#   DBI::dbWriteTable(conn = conn, name = name, value = object, overwrite=overwrite, append=append)
#   DBI::dbDisconnect(conn)
#   message(paste0(Sys.time(), " objekti ", name, " tallennettu ",database,"-kantaan."))
# }
