#' @title  Get metadata of the table
#' @description Function returns metadata of the table from _V_RELATION_COLUMN view.
#' @param table name of the table
#' @param database 'DMPROD00' / 'EDWPROD' / 'STAGEPROD'
#' @author Jani Miettinen <jani.k.miettinen@gmail.com>
#' @return data frame
#' @examples
#'  metadata(table = "D_ASIAKAS", database = "DMPROD00")
#' @rdname metadata
#' @export


metadata <- function(table, database){
  connect <- dbtools::dw_connection(database)
  d <- odbc::dbGetQuery(connect, paste0("select NAME, ATTNAME, DESCRIPTION, FORMAT_TYPE from _V_RELATION_COLUMN where NAME = '", table,"' limit 10; "))
  dbtools::dw_disconnect(connect)
  return(d)
}
