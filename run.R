# copy docs to public_html
if("Linux" %in% Sys.info()){
  devtools::document()
  options(pkgdown.internet = FALSE)
  pkgdown::build_site(new_process = FALSE)
  # pkgdown::build_site()
  ohjeen_kansio <- "~/public_html/rpackages/dbtools/"
  if (!dir.exists(ohjeen_kansio)) dir.create(ohjeen_kansio)
  system(paste0("rsync -arv --delete ./docs/* ", ohjeen_kansio))
}

# windowssissa?
if("Windows" %in% Sys.info()){
  print(paste0(Sys.time(), " en tehnyt mitään."))
}
