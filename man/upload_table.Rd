% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/upload_table.R
\name{upload_table}
\alias{upload_table}
\title{Upload dataframe to MariaDB}
\usage{
upload_table(object, group = NULL, overwrite = F, append = T)
}
\arguments{
\item{object}{data frame}

\item{group}{database connection}

\item{overwrite}{overwrite table in database}

\item{append}{add rows to table in database}
}
\description{
Message print to console with or without timestamp
}
\examples{
upload_table()

}
\author{
Jani Miettinen <jani.k.miettinen@gmail.com>
}
