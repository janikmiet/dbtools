% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/get_query.R
\name{get_query}
\alias{get_query}
\title{Get sql query}
\usage{
get_query(sql, group = "STATTLESHIP")
}
\arguments{
\item{sql}{string. SQL query}

\item{group}{database connection alias}
}
\value{
dataframe
}
\description{
returns outcome of sql query from database
}
\examples{
 \dontrun{
 get_query("SELECT 1")
 }
}
\author{
Jani Miettinen
}
